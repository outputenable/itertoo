#
# SPDX-FileCopyrightText: © 2021 Oliver Ebert <oe@outputenable.net>
#
# SPDX-License-Identifier: Zlib
#

"""itertoo is `itertools`_ without ls but more.

* :func:`isiterable`: Determine whether object is iterable.
* :func:`try_iter`: Get :func:`iter` or :obj:`None`.

* :func:`ilen`: Determine the "length" of an iterable.

* :func:`repeat_last`: Yield from iterable, eventually repeating the last
  item.

* :func:`flatten`: Flatten nested iterables depth-first.

.. _itertools: https://docs.python.org/3/library/itertools.html
"""

import sys

if sys.version_info < (3, 5):
    raise RuntimeError(__file__ + " requires Python >= 3.5")

from itertools import *
from typing import Any, Callable, Iterable, Iterator, List, Optional


_SENTINEL = object()


def isiterable(x: Any) -> bool:
    """Return whether `x` is iterable or not.

    Args:
        x: Any object.

    Returns:
        :obj:`True` if ``iter(x)`` succeeds, :obj:`False` otherwise.
    """
    try:
        iter(x)
    except TypeError:
        return False
    else:
        return True


def try_iter(*args) -> Optional[Iterator]:
    """Try to return ``iter(*args)`` or if that raises :class:`TypeError`,
    return :obj:`None`.

    Args:
        args: Arguments for :func:`iter`.

    Returns:
        ``iter(*args)`` or :obj:`None`.
    """
    try:
        return iter(*args)
    except TypeError:
        return None


def ilen(iterable: Iterable) -> int:
    """Return the length (the number of items) of the given iterable.

    Args:
        iterable: An iterable.

    Returns:
        The number of items in `iterable`.

    If the given iterable has a :attr:`__len__` attribute then call that to
    determine its length.  Otherwise actually iterate over `iterable` and
    count the number of items.

    Examples:
        >>> ilen([1, 2, 3])
        3
        >>> len(iter([1, 2, 3]))
        Traceback (most recent call last):
          ...
        TypeError: object of type 'list_iterator' has no len()
        >>> ilen(iter([1, 2, 3]))
        3
        >>> ilen(iter([]))
        0
    """
    __len__ = getattr(iterable, "__len__", None)
    if __len__:
        return __len__()
    i = (0, None)
    for i in enumerate(iterable, 1):
        pass
    return i[0]


def repeat_last(iterable: Iterable, *defaults: Any) -> Iterator:
    """Yield from `iterable` or if that is empty, from `defaults`, and then
    repeatedly yield the last item, if any.

    Args:
        iterable: An iterable.
        defaults: Items to yield if `iterable` is empty.

    Yields:
        Items from `iterable` or if there are none, from `defaults`, and then
        repeatedly the last item, if any.

    Examples:
        >>> list(repeat_last([]))
        []
        >>> list(islice(repeat_last([], 0), 5))
        [0, 0, 0, 0, 0]
        >>> list(islice(repeat_last([1, 2, 3]), 5))
        [1, 2, 3, 3, 3]
        >>> list(islice(repeat_last([1, 2, 3], 4), 5))
        [1, 2, 3, 3, 3]
    """
    item = _SENTINEL
    for item in iterable:
        yield item
    if item is _SENTINEL:
        if defaults:
            for item in defaults:
                yield item
        else:
            return
    yield from repeat(item)


def flatten(
    x: Any, levels: int = -1, try_iter: Callable[[Any], Optional[Iterator]] = try_iter
) -> Iterator:
    """Lazily remove up to `levels` of nesting, depth-first, from the given
    (iterable) object `x`.

    Args:
        x: Any object.
        levels: The maximum number of (nested) iterables that should be
            flattened; that is, up to `levels` of nesting will be removed from
            `x`.  A value of zero will just yield `x` again while any negative
            number results in unbounded flattening.
        try_iter: A callable that returns either an iterator from its argument
            or a Falsy value to indicate that the argument is not iterable.

    Yields:
        Items from `x` depth-first up to `levels` deep.

    This is basically an extended :meth:`itertools.chain.from_iterable`.

    Examples:
        >>> list(flatten(1))
        [1]
        >>> list(flatten([1]))
        [1]
        >>> list(flatten([1], 0))
        [[1]]
        >>> list(flatten([1, 2, 3, 4]))
        [1, 2, 3, 4]
        >>> list(flatten([[[[[[42]]]]]]))
        [42]
        >>> list(flatten([[1], [[2]], [[[3]]], [[[[4]]]]]))
        [1, 2, 3, 4]
        >>> list(flatten([1, [2, [3, [4, [5, [6, [7, [8, [9]]]]]]]]], 5))
        [1, 2, 3, 4, 5, [6, [7, [8, [9]]]]]
        >>> list(flatten(
        ...     [['abc'], ['def']],
        ...     try_iter=lambda x: None if isinstance(x, str) else try_iter(x)))
        ['abc', 'def']
        >>> list(flatten(
        ...     ['a', ['bc'], [['def']]],
        ...     try_iter=lambda x: None if isinstance(x, str) and len(x) == 1 else try_iter(x)))
        ['a', 'b', 'c', 'd', 'e', 'f']
    """
    if not levels:
        yield x
    else:
        # Now that we know that we're (possibly) going to remove at least this
        # one (current) level of nesting, we'll just keep track of what number
        # of `levels` remain for (potential) nested items.
        levels -= 1
        i = iter((x,))
        stack: List[Iterator] = []
        while True:
            for x in i:
                ix = try_iter(x)
                if not ix:
                    yield x
                elif not levels:
                    # Since no further nesting levels will be removed from the
                    # items in `ix` anyway we don't need to descend and
                    # process each one individually but can instead simply
                    yield from ix
                else:
                    levels -= 1
                    stack.append(i)
                    i = ix
                    break
            else:
                if stack:
                    i = stack.pop()
                    levels += 1
                else:
                    break


if __name__ == "__main__":
    import doctest

    doctest.testmod()
