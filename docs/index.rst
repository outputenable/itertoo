.. itertoo documentation master file, created by
   sphinx-quickstart on Wed Mar 10 00:45:26 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to itertoo's documentation!
===================================

.. toctree::
   :maxdepth: 4
   :caption: Contents:

   itertoo


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
