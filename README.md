# itertoo

itertoo is Python's itertools without ls but more.

[![No Maintenance Intended](http://unmaintained.tech/badge.svg)](http://unmaintained.tech/)

Programmers who viewed this item also viewed:

* [more-itertools](https://pypi.org/project/more-itertools/)
* [toolz/PyToolz](https://pypi.org/project/toolz/)
* [Pytoolz](https://pypi.org/project/pytoolz/)
